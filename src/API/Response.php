<?php

namespace d3x\ajpes\API;


class Response
{
    public $parsedResponse;

    public function __construct($raw)
    {
        $res = htmlspecialchars_decode($raw);
        $xmlElement = simplexml_load_string($res);
        $response = strtr($res, ['</soap:' => '</', '<soap:' => '<']);
        $this->parsedResponse = json_decode(json_encode(simplexml_load_string($response)));
    }

    public function getData()
    {
        return $this->parsedResponse->Body->oddajPorociloResponse->oddajPorociloResult->data;
    }

    public function getRows()
    {
        $rows = $this->getData()->row;

        // Check if $rows is an object, and wrap it in an array if it is
        if (is_object($rows)) {
            $rows = [$rows];
        }

        return array_map(static fn($r) => $r->{'@attributes'}, $rows);
    }

    public function getAttributes()
    {
        return $this->getData()->{'@attributes'};
    }

    public function getSuccess()
    {
        return $this->getAttributes()->success;
    }

    public function getFailure()
    {
        return $this->getAttributes()->failure;
    }

    public function getFailureRows()
    {
        return $this->getAttributes()->failureRows;
    }


}
