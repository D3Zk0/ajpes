<?php

namespace d3x\ajpes\API\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class AjpesException extends Exception
{
    public function __construct($config, $message, $code)
    {
        Log::error("AJPES ERROR" . $message, compact("config", "code"));
        parent::__construct($message, $code);
    }
}
