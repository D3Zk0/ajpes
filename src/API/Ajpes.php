<?php

namespace d3x\ajpes\API;

use d3x\ajpes\API\Exceptions\AjpesException;
use Illuminate\Support\Facades\View;

class Ajpes
{
    protected $WSDL;
    protected $certificate;
    protected $guests;
    protected const HEADERS = [
        "Content-Type: text/xml;charset=UTF-8,",
        'SOAPAction: "http://www.ajpes.si/eturizem/oddajPorocilo"',
        "Host: wwwt.ajpes.si"
    ];

    public function __construct($guests = null)
    {
        if (config('ajpes.env') === "dev") {
            $this->WSDL = config("ajpes.test_wsdl");
//            $this->certificate = config("ajpes.certificate");
            $this->certificate = __DIR__ . "/Certs/AjpesTest.pem";

        } else if (config('ajpes.env') === "prod") {
            $this->WSDL = config("ajpes.wsdl");
            $this->certificate = config("ajpes.certificate");
            $this->guests = $guests;
        }
    }

    private function getOptions()
    {
        $postfields = View::make('ajpes::xml_template', ["guests" => $this->guests])->render();
        return [
            CURLOPT_URL => $this->WSDL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "gzip,deflate",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => self::HEADERS,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSLCERT => $this->certificate,
            CURLOPT_SSLCERTPASSWD => config("ajpes.certificate_password"),
        ];
    }

    public function report()
    {
        $curl = curl_init();
        $config = $this->getOptions();
        curl_setopt_array($curl, $config);

        $response = curl_exec($curl);
        if ($response === false)
            throw new AjpesException($config, curl_error($curl), curl_errno($curl));
        curl_close($curl);
        return new Response($response);
    }
}
