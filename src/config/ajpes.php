<?php
return [
    "env" => env("AJPES_ENV", "dev"),
    "wsdl" => "https://www.ajpes.si/wsrno/eTurizem/wsETurizemPorocanje.asmx?WSDL",
    "test_wsdl" => "https://wwwt.ajpes.si/rno/rnoApi/eTurizem/wsETurizemPorocanje.asmx",
    "username" => env("AJPES_USERNAME", "apiTest"),
    "password" => env("AJPES_PASSWORD", "Test123!"),
    "certificate" => env("AJPES_CERTIFICATE", "/Certs/AjpesTest.pem"),
    "certificate_password" => env("AJPES_CERTIFICATE_PASSWORD", "ajpestest"),
    "format" => 1,
];


