<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:etur="http://www.ajpes.si/eturizem/">
    <soapenv:Header/>
    <soapenv:Body>
        <etur:oddajPorocilo>
            <etur:uName>{{ config("ajpes.username") }}</etur:uName>
            <etur:pwd>{{ config("ajpes.password") }}</etur:pwd>
            <etur:data>
                @if(config("ajpes.env") == "dev")
                    @include("ajpes::test_data")
                @elseif(config("ajpes.env") == "prod" && isset($guests))
                    @include("ajpes::data_builder", $guests)
                @endif
            </etur:data>
            <etur:format>
                {{ config("ajpes.format") }}
            </etur:format>
        </etur:oddajPorocilo>
    </soapenv:Body>
</soapenv:Envelope>
