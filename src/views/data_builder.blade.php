<knjigaGostov>
    @foreach($guests as $guest)
        <row status="{{$guest->status}}"
             ttVisina="{{$guest->ttVisina}}"
             ttObracun="{{$guest->ttObracun}}"
             @if($guest->casOdhoda)
                 casOdhoda="{{$guest->casOdhoda}}"
             @endif
             casPrihoda="{{$guest->casPrihoda}}"
             idStDok="{{$guest->idStDok}}"
             vrstaDok="{{$guest->vrstaDok}}"
             drzava="{{$guest->drzava}}"
             dtRoj="{{$guest->dtRoj}}"
             sp="{{$guest->sp}}"
             pri="{{$guest->pri}}"
             ime="{{$guest->ime}}"
             zst="{{$guest->zst}}"
             idNO="{{$guest->idNO}}"/>
    @endforeach
</knjigaGostov>
