<?php

namespace d3x\ajpes\Functions;

use Exception;

class Functions
{
    public
    static function p12ToPemSave($path, $password)
    {
        $res = [];
        $openSSL = openssl_pkcs12_read(file_get_contents($path), $res, $password);
        if (!$openSSL) {
            throw new Exception("Error: " . openssl_error_string());
        }
        // this is the PEM FILE
        $cert = $res['cert'] . $res["pkey"] . implode('', $res['extracerts']);
        $path = str_replace(".bin", ".pem", $path);
        file_put_contents($path, $cert);
        return $path;
    }
    public static function validateCertificatePassword($certPath, $password)
    {
        if (!$cert = file_get_contents($certPath)) {
            throw new Exception("Certifikat " . $certPath . " ne obstaja!");
        }
        return openssl_pkcs12_read($cert, $cert_info, $password);
    }
}