<?php

namespace d3x\ajpes;

use Illuminate\Support\ServiceProvider;


class AjpesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {

        }
        $this->loadViewsFrom(__DIR__.'/views', 'ajpes');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/ajpes.php', 'ajpes'
        );
    }
}
